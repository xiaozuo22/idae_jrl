package com.fast.dev.ucenter.service.config;

import com.fast.dev.component.mongodb.conf.MongodbConfig;
import com.fast.dev.component.mongodb.configuration.MongodbConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fast.dev.component.mongodb")
public class DBConfig {


}
